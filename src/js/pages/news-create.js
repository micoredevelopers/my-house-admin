import { handleAddImage } from '../helpers'

const blockTemplate = (index) => `
  <div class="row mb-5 news-row">
    <div class="col-12">
      <div class="row photos-wrap">
        <div class="col-3 mb-4">
          <div class="form-group add-file">
            <label for="news-photos-${index}" class="add-icon">
              <span></span>
              <span></span>
            </label>
            <input class="form-control-file" type="file" accept="image/*" multiple 
                  id="news-photos-${index}" data-img-size="big" data-img-type="news">
          </div>
        </div>
      </div>
    </div>
    <div class="col-12">
      <div class="form-group text-area">
        <textarea class="form-control" name="news-text-${index}" rows="6" placeholder="Текст новости"></textarea>
      </div>
    </div>    
  </div>
`

$('.add-news-block').on('click', function () {
  const rowInputs = $(this).parents('.inputs')
  const rowCount = rowInputs.find('.news-row').length

  rowInputs.append(blockTemplate(rowCount))

  $('.form-control-file').on('change', handleAddImage)
})
