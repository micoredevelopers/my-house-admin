const deleteImage = () => {
  $('[data-action="delete"]').on('click', function () {

    if($('main').attr('id') == 'partners-page') {
      $(this).parents('.col-2').remove();
    } else {
      $(this).parent().parent().find('.img-preview').remove();
      $(this).parent().find('.input-preview_file').val('');
      $(this).parent().parent().removeClass('preview-active');
    }

  })

}

deleteImage();

const viewImage = () => {
  $('.view').on('click', function () {
    const imgSrc = $(this).parent().parent().find('.img-preview').attr('src');
    const photoWrap = $('.modal-photo-image')

    photoWrap.html('')
    photoWrap.append(`<img src="${imgSrc}" alt="Modal photo"/>`)
  })
}

const editImage = () => {
  $('.edit').on('change', function (e) {
    const input = e.target
    const image = $(this).parents().parent().find('.inputs__photo img')

    if (input.files[0]) {
      const reader = new FileReader()

      reader.onload = function (e) {
        const src = e.target.result

        image.attr('src', src)
      }

      reader.readAsDataURL(input.files[0])
    }
  })
}

const handleAddImage = (e) => {
  const input = e.target
  const submitBtn = $('button[type=submit]')
  const size = $(input).attr('data-img-size')
  const type = $(input).attr('data-img-type')
  const photosRow = type === 'news' ? $(input).parents('.photos-wrap') : $('.photos-wrap')

  if (submitBtn.hasClass('d-none')) {
    submitBtn.removeClass('d-none')
  }

  // photosRow.html('')

  // Object.values(input.files).map((file, index) => {
  //   const reader = new FileReader()
  //
  //   reader.onload = function (e) {
  //     const src = e.target.result
  //     const photoTemplate = `
  //       <div class="${(type === 'certificate' || type === 'news')
  //                     ? 'col-3 mb-4' : type === 'partners'
  //                     ? 'col-3 mb-5' : size === 'big'
  //                     ? 'col-4 mb-4' : 'col-2 mb-4'}">
  //         <div class="inputs__photo-preview mb-3 ${size === 'big' ? 'inputs__photo-preview_big' : 'inputs__photo-preview_small'}">
  //           <div class="inputs__photo-actions">
  //             ${size === 'small' ? `
  //               <button class="action-btn view" data-toggle="modal" data-target="#photo-modal" data-src="${src}">
  //                  <img class="icon-action" src="img/icons/ViewIcon.svg" alt="View">
  //               </button>
  //             ` : ''}
  //             <div class="form-group action-btn edit">
  //               <label for="edit-photo-${index + 1}">
  //                 <img class="icon-action" src="img/icons/EditIcon.svg" alt="Edit">
  //                 ${size === 'big' ? `<span>Редактировать</span>` : ''}
  //               </label>
  //               <input type="file" accept="image/*" id="edit-photo-${index + 1}" class="edit">
  //             </div>
  //             <button class="action-btn delete">
  //                <img class="icon-action" src="img/icons/DeleteIcon.svg" alt="Delete">
  //                ${size === 'big' ? `<span>Удалить</span>` : ''}
  //             </button>
  //           </div>
  //           <div class="inputs__photo">
  //             <img src="${src}" alt="${file.name}">
  //           </div>
  //         </div>
  //         ${type === 'certificate' ? `
  //           <div class="form-group">
  //             <input type="text" class="form-control" name="certificate-name-${index + 1}" placeholder="Название сертификата">
  //           </div>
  //         ` : ''}
  //       </div>
  //     `
  //
  //     type === 'news'
  //       ? photosRow.prepend(photoTemplate)
  //       : photosRow.append(photoTemplate)
  //
  //     viewImage()
  //     editImage()
  //     deleteImage()
  //   }
  //
  //   reader.readAsDataURL(file)
  // })
}

$('.header__collapse').on('click', function () {
  $('.header').toggleClass('is-collapsed')
})

$('.main-form').on('submit', function (e) {
  e.preventDefault()

  $(this).find('input').map((index, input) => {
    console.log({ name: $(input).attr('name'), value: $(input).val() })
  })
})

$('.dropdown-item.delete').on('click', function () {
  const tBody = $(this).parents('tbody')
  const deleteRowTemplate = `
    <tr>
      <td colspan="5">
        <div id="form-delete">
          <form action="/">
            <p class="delete-text">Вы уверены что хотите удалить «Это наш лучший проект за неделю»?</p>
            <a href="#" class="delete-btn">Да</a>
            <a href="javascript:void[0]" class="cancel-btn">Отмена</a>
          </form>
        </div>
      </td>    
    </tr>
  `

  tBody.prepend(deleteRowTemplate)

  $('.cancel-btn').on('click', function () {
    $(this).parents('tr').remove()
  })
})

$('.form-control-file').on('change', handleAddImage)

viewImage()
editImage()
deleteImage()

//tables
const table = $('.objects-tbody');
const tablesBtn = $('.drag-icon_col');
const fTable = $('.feedback-tbody');

//objects
const objects = $('.created-object_wrapper');
const objectsBtn = $('.btn-object-drag');
//floors
const floors = $('.flat-floor_wrapper');
const floorsBtn = $('.flat-floor_btn-drag');
//menu
const adminMenu = $('.menu-wrapper');
const menubtns = $('.menu-drag_item');
//flat
const flats = $('.flat-wrapper-drag');
const flatsBtn = $('.flat-wrapper-show .flat-floor_btn-drag');

function dragElem(wrapper) {

  if(wrapper === flats) {
    wrapper.sortable({
      axis: 'x',
      handle:'.drag-icon',
      placeholder: 'placeholderBackground',
    })
  } else {
    wrapper.sortable({
      handle:'.drag-icon',
      placeholder: 'placeholderBackground',
    })
  }
}

dragElem(table,tablesBtn);
dragElem(objects,objectsBtn);
dragElem(floors,floorsBtn);
dragElem(fTable, tablesBtn);
dragElem(adminMenu,menubtns);
dragElem(flats,flatsBtn);

const inputPreview = $('.input-preview_file');

function readURL(input) {
  if (input.files&&input.files[0]) {
    let reader = new FileReader();
    if($(input).parents('.add-file').find('.img-preview')) {
      $(input).parents('.add-file').find('.img-preview').remove();
    }
    reader.onload = function(e) {
      $(input).parents('.add-file').append(`
        <img class="img-preview" src="${e.target.result}">
      `);
    }

    reader.readAsDataURL(input.files[0]); // convert to base64 string

    $(input).parents('.add-file').addClass('preview-active');

  }
}

inputPreview.change(function() {
  readURL(this);
});


function checkObjects() {

  if($('.private-houses_wrap')) {
    $('.create-object_objects-footer').addClass('object-exists');
  }

  $('.create-object_objects-wrapper').on('change', function () {
    if($('.private-houses_wrap')) {
      $('.create-object_objects-footer').addClass('object-exists');
    } else {
      $('.create-object_objects-footer').removeClass('object-exists');
    }
  })

}

checkObjects();
const button = $('.create-object_object-menu');
const statusBtn = $('.card-status_item-menu');
function openObjectMenu(button) {


  button.on('click', function () {
    const menu = $(this).parent().find('ul');

    menu.toggleClass('object-menu-active');
  })
}

openObjectMenu(button);
openObjectMenu(statusBtn);
//closing all active menu

$('body').on('click', function (event) {
  const target = event.target;
  const menu = $('.object-head_menu');
  const menu2 = $('.card-status_menu');
  if(!($(target).hasClass('create-object_object-menu'))) {
    if(menu.hasClass('object-menu-active')) {
      menu.removeClass('object-menu-active');
    }
  }
  if(!($(target).hasClass('card-status_item-menu'))) {
    if(menu2.hasClass('object-menu-active')) {
      menu2.removeClass('object-menu-active');
    }
  }
})


function showObjects() {
    const btn = $('.create-object_object-expander');
    const floorBtn = $('.flat-floor_expander');
    btn.on('click',function () {
      let elem = null;
      let wrapper = null;
      if($(this).parents('.create-object_objects-wrapper')) {
        wrapper = $(this).parents('.create-object_objects-wrapper');
      }

        if(wrapper.find('.created-object')) {
          elem = wrapper.find('.created-object');
        } else if(wrapper.find('.flat-floor_wrapper')) {
          elem = wrapper.find('.flat-floor_wrapper');
        }


      if(elem) {
        $(this).toggleClass('expander-active');
        elem.slideToggle('slow');
        elem.toggleClass('object-active');
      }

    })

  floorBtn.on('click', function () {
   const elem = $(this).parents('.show-flat').find('.flat-wrapper');
    console.log(elem)
    $(this).toggleClass('expander-active');
    elem.slideToggle('slow');
    elem.toggleClass('object-active');
  })
}

showObjects();

//modalVideo
function openVideo() {
  const videoBtns = $('.video-td_btn');
  const closeModal = $('#videoModal .close-btn');
  let src = null;
  let iframe = null;


  videoBtns.on('click', function () {
     src = $(this).attr('data-modal-video');
     iframe = $('#videoModal .embed-responsive');
     if(iframe.find('.embed-responsive-item')) {
       iframe.find('.embed-responsive-item').remove();
     }
     if(iframe.find('.no-video')) {
       iframe.find('.no-video').remove();
     }

    if(src !== '') {
      iframe.append(`<iframe class="embed-responsive-item" src="${src}"   frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`)
    } else {
      iframe.append('<div class="no-video"><p>Видео отсутствует</p></div>');
    }
  })

  closeModal.on('click', function () {
    if(src) {
      iframe.find('.embed-responsive-item').remove();
    }
  })

}

openVideo();

function menuSwitch() {
  const items = $('.menu-switcher, .create-object_switch2');

  items.on('change', function () {
    const label = $(this).parent().find('label');
    console.log($(this).prop('checked'));
    if($(this).prop('checked')) {
      label.css('color', '#222');
    } else {
      label.css('color', 'rgb(180,180,180)');
    }
  })
}

menuSwitch();

export { handleAddImage }
