require('./helpers')



const main = document.querySelector('main')
const id = main.getAttribute('id')

switch (id) {
  case 'login-page':
    require('./pages/login')
    break
  case 'fleet-create-page':
    require('./pages/fleet-create')
    break
  case 'projects-create-page':
    require('./pages/fleet-create')
    break
  case 'news-create-page':
    require('./pages/news-create')
    break
  case 'object-create-about':
    $('#create-object_switch-about').addClass('active');
    break
  case 'object-create-objects':
    $('#create-object_switch-objects').addClass('active');
    break
  case 'object-create-status':
    $('#create-object_switch-status').addClass('active');
    break;
  case 'object-create-gallery':
    $('#create-object_switch-gallery').addClass('active');
    break;
  case 'texts-page':
    $('#texts-menu_main').addClass('active');
    break;
  case 'texts-about':
    $('#texts-menu_about').addClass('active');
    break;
  case 'texts-building':
    $('#texts-menu_building').addClass('active');
    break;
  case 'texts-renovation':
    $('#texts-menu_renovation').addClass('active');
    break;
  case 'texts-feedback':
    $('#texts-menu_feedback-form').addClass('active');
    break;
  case 'seo-page':
    $('#seo-sections').addClass('active');
    break;
  case 'seo-robot_page':
    $('#seo-sitemap').addClass('active');
    break;
  case 'seo-head_page':
    $('#seo-head').addClass('active');
    break;

}
